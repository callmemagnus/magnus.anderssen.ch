FROM node:lts-alpine AS builder

ARG phone
ARG email

RUN mkdir /app
WORKDIR /app
COPY . /app/
RUN npm ci && npm run build

FROM callmemagnus/nginx4static:1.0
WORKDIR /usr/share/nginx/html
COPY --from=builder /app/dist .
