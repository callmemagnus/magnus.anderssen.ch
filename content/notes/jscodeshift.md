---
title: Refactoring tooling
date: 2017-06-02
---

## jscodeshift

The `jscodeshift` is an interesting piece of work.

It is a transformation runner, it enables to write automated code upgrade tools.

Facebook uses it in order to upgrade the React application to follow the API versions.

- [jscodeshift on github](https://github.com/facebook/jscodeshift)

## Example

To automate the upgrade of your createComponent based React components:

```shell
npm i -g jscodeshift
git clone https://github.com/reactjs/react-codemod.git
cd react-codemod
npm i
```

Then to perform a transformation on your code:

```shell
jscodeshift -t react-codemod/transforms/class.js path/to/components
```

This will "magically" upgrade you React component to a ES2016 class [React upgrade guidelines](https://facebook.github.io/react/blog/2017/04/07/react-v15.5.0.html#migrating-from-react.createclass).
