---
title: RXJS
date: 2018-04-06
---

## event stream

```js
const Observable = Rx.Observable;

const x = Observable.fromEvent(document.getElementById('button'), 'click')

x.forEach(y => alert('clicked'))

x.forEach(
  function onNext(e) {...},
  function onError(e) {...},
  function onCompleted() {...}
)
```

To unsubscribe:

```js
const x = Observable.fromEvent(document.getElementById('button'), 'click')

const sub = x.forEach(function(e) {
  ...
  sub.dispose()
}
```

## Stream transformation

```js
const x = Observable.fromEvent(document.getElementById('button'), 'click');

const points = x.map(function (e) {
	return { x: e.clientX, y: e.clientY };
});
```

This is not triggering any operation as long as you don't consume it by calling `.forEach`.

## Available operation on event streams

### .scan

Like `reduce`on array, takes a function with accumulator as first parameter and the current item value in the second.

```js
var button = document.querySelector('button');
Rx.Observable.fromEvent(button, 'click')
	.scan(count => count + 1, 0)
	.subscribe(count => console.log(`Clicked ${count} times`));
```

### Flow control operators

TODO

- filter --
- delay
- debounceTime
- take
- takeUntil
- distinct
- distinctUntilChanged

### Value producing operators

- pluck
- pairwise
- sample
