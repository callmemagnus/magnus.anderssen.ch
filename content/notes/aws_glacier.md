---
title: Using AWS glacier
date: 2017-03-07
tags:
  - aws
  - shell
---

This page contains my notes on AWS glacier CLI.

## AWS setup

```shell
aws configure
```

## Create a vault

```shell
aws glacier create-vault --account-id - --vault-name Photos
```

## Upload a file

Create an archive, a file in the vault.

Then compute its digest with `openssl`.

```shell
openssl dgst -sha256 $FILENAME
```

Then the file can be uploaded:

```shell
aws glacier upload-archive \
    --account-id - \
    --vault-name $VAULTNAME \
    --checksum $CHECKSUM \
    --body $FILENAME \
    --archive-description "XXX"
```

## TODO Multi-chunks upload

When the archive is big, more than XXX, the file must be split.

```shell
split ...
```

## Get The list of archives in vault

```shell
aws glacier initiate-job  --account-id - --vault-name Photos --job-parameters '{"Type": "inventory-retrieval"}'
{
    "jobId": "FMv8wdcihQ-eTFxBP8DSyjkr4I1-oyjOPRNnehFpFv_wTkb9extLFoCzX1v0erJFwB40nj0Hml3qdTw0szqYJ0s1Eln0",
    "location": "/986072598122/vaults/Photos/jobs/FMv8wdcihQ-eTFxBP8DSyjkr4I1-oyjOPRNnehFpFv_wTkb9extLFoCzX1v0erJFwB40nj0Hml3qdTw0szqYJ0s1Eln0"
}
```

## get the status of a initiated job

```shell
aws glacier list-jobs --account-id - --vault-name Photos
{
    "JobList": [
        {
            "Action": "InventoryRetrieval",
            "CreationDate": "2017-12-30T18:38:41.400Z",
            "Completed": false,
            "StatusCode": "InProgress",
            "InventoryRetrievalParameters": {
                "Format": "JSON"
            },
            "VaultARN": "arn:aws:glacier:eu-central-1:986072598122:vaults/Photos",
            "JobId": "FMv8wdcihQ-eTFxBP8DSyjkr4I1-oyjOPRNnehFpFv_wTkb9extLFoCzX1v0erJFwB40nj0Hml3qdTw0szqYJ0s1Eln0"
        }
    ]
}
```

## get the result of the initiated job

```shell
aws glacier get-job-output --account-id - --vault-name Photos --job-id $JOB_ID outfile test.json
```
