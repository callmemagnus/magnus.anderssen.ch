---
title: Paralax with jQuery
tags:
  - parallax
  - javascript
date: 2013-02-07
publish: true
---

Uses jQuery ;-)

```js
//
// Parallax
//

function parallax(zone, layers) {
	this.activeZone = zone;
	this.layers = layers;
}

parallax.prototype.init = function () {
	var self = this;
	self.activeZoneHeight = self.activeZone.height();
	self.activeZoneTop = self.activeZone.offset().top;

	$(self.layers).each(function (i, layer) {
		layer.top = parseInt(layer.item.css('top'));
	});

	self.resize();

	$(window).resize(function () {
		self.resize();
	});

	$(window).scroll(function () {
		self.moveLayers();
	});
};

parallax.prototype.moveLayers = function () {
	var self = this;

	self.topPos = $(window).scrollTop();
	self.ratio = Math.min(1, Math.max(0, (self.topPos - self.baseDisplacement) / self.maxDisplacement));

	$(self.layers).each(function (i, layer) {
		layer.item.css({
			top: Math.round(layer.top - layer.displacement * self.ratio + layer.displacement / 2) + 'px'
		});
	});
};

parallax.prototype.resize = function () {
	var self = this;

	self.maxDisplacement = scroll.windowHeight + self.activeZoneHeight;
	self.baseDisplacement = self.activeZoneTop - scroll.windowHeight;

	self.moveLayers();
};

var ecolaborationParallax = new parallax($('.art-block.intro'), [
	{
		item: $('.art-block.intro .leaf-1'),
		displacement: 400
	},
	{
		item: $('.art-block.intro .leaf-2'),
		displacement: 200
	},
	{
		item: $('.art-block.intro .leaf-3'),
		displacement: 400
	}
]);
```
