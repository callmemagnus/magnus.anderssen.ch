---
title: Publish module on NPM
date: 2018-06-06
---

## setup

```shell
$ git init
... configure a remote (git remote add)
... add stuff and commit
$ npm init
```

Install `babel-cli` and `babel-latest` and add the 'build' script `babel src -d build` to the `package.json`.

In the same file, we'll need to configure babel:

```json
{
    ...
    "babel": {
        "presets": "latest"
    }
    ...
}
```

Run `npm run build`.

The `watch` package can be used to run the task on file changes. Add a 'dev' script `watch 'npm run build'`. Note may not work on windows.

`jest` can also be added to handle tests. Add 'test' task `jest`.

Add 'file.test.js' and it will be recognized by jest.

```js
import testModule from '.';

test('works'. () => (
    expect(testModule.work()).toBe('hell yeah');
));
```

Jest can be ran automatically on file updates: `jest --watch`.

## Testing packages locally (npm link)

First run `npm link` in the developed module folder. Then in the project that uses the module, run `npm link <name of the module>`. This will create a symbolic link link to the local version.

## Don't forget the Readme

## prepublish

Add a script name 'prepublish' running the tasks that are required to build the module.

It will be ran before the publish task.

Add a `.npmignore` containing the list of directories and files to ignore.

```
src
```

NPM will ignore files in `.gitignore`and `files in`.npmignore`.

## prior to publish

Use your username password to connect npm to npmjs:

```shell
$ npm adduser
```

You are now connected.

## Publish

```shell
$ npm publish
```

### Scoped packages

It seems to be the new trend, add scope to your package name. Every user of npm has the scope of its username.

If you run `npm publish`, it will fail miserably with an error:

```
62 error code E402
63 error You must sign up for private packages : @magnus/send-timings
```

This is because scoped packages are private by default; that is a billable feature of npm.

This is solved by publishing publicly:

```shell
npm publish --access=public
```

## Update existing module

Install 'np', add a task name 'release' executing `np` to handle the upgrades.
