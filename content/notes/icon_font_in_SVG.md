---
title: Create an SVG Icon System
date: 2017-12-01
---

## Optimize SVGs

Install `svgo` from npm then `svgo in.svg -o out.svg`

Or run on a folder `svgo -f dir -o out`

With plugins: `svgo -f dir -o out -p 1 --multipass --enable='removeTitle,removeDesc, cleanupIDs,removeDimensions`

## SVG as background

```html
<i class="Icon Icon--star"></i>
```

```css
.Icon {
	height: 3em;
	width: 3em;
	display: inline-block;
}

.Icon--star {
	background-image: url(path/to/svg);
	transition: background-image ease 0.2s;
}

.Icon--star:hover {
	background-image: url(path/to/hover-svg);
}
```

## SVG embedded

```css
.Class {
	background-image: utl(data: image/svg + xml);
}
```

## Generate "sprite"

Install `svg-sprite-generator`. After the svgo completed correctly, run `svg-sprite-generator -d in/dir -o out/dir`.

To use the result:

```html
<i class="Icon Icon--thing"></i> <i class="Icon Icon--other-thing"></i>
<i class="Icon Icon--thing Icon--small"></i>
<i class="Icon Icon--other-thing Icon--small"></i>
```

```css
.Icon {
	width: 3em;
	height: 3em;
	display: inline-block;
	background-image: url(path/to/svg/sprite);
	background-repeat: no-repeat;
	background-size: cover;
}
.Icon--small {
	height: 2em;
	width: 2em;
}
.Icon--thing {
	background-position: 0 0;
}
.Icon--other-thing {
	background-position: 0 -3em; /* based on the height */
}
.Icon--other-thing.Icon--small {
	background-position: 0 -2em;
}
```

With sccs mixins:

```css
$icon: thing, other-thing;
$defaultSize: 3em;
$smallSize: 2 .Icon {
	width: $defaultSize;
	height: $defaultSize;
	display: inline-block;
	background-image: url(path/to/svg/sprite);
	background-repeat: no-repeat;
	background-size: cover;
}
.Icon--small {
	height: $smallSize;
	width: $smallSize;
}

@each $icon in $icons {
	$index: index($icons, $icon) -1; /* sass index starts at 1 */

	.Icon--#{$icon} {
		background-position: 0 #{-$index * $defaultSize};

		&.Icon--small {
			background-position: 0 #{-$index * $smallSize};
		}
	}
}
```
