---
title: Using GPG
date: 2014-03-04
tags:
  - gpg
  - cli
---

## generate key

```shell
gpg --gen-key
```

## list key

```shell
gpg --list-keys
```

## sign

```shell
gpg --sign name
```

## encrypt and sign

```shell
gpg --sign --encrypt [--armor] name
```

## extract content from signed file

```shell
gpg -v file
```

or

```shell
gpg -d file
```

## analysis

```shell
gpg --list-packets --list-only file
```

### encrypted example

```
:pubkey enc packet: version 3, algo 16, keyid C5AC471B44F5C28F
    data: [4093 bits]
    data: [4096 bits]
:pubkey enc packet: version 3, algo 1, keyid 5C282F9C6F7A0B5E
    data: [2048 bits]
:pubkey enc packet: version 3, algo 16, keyid 862787EA0E886576
    data: [2046 bits]
    data: [2048 bits]
:encrypted data packet:
    length: 3191
    mdc_method: 2
gpg: encrypted with 2048-bit RSA key, ID 44F5C28F, created 2010-06-15
    "John Doe <john@example.com>"
gpg: encrypted with 4096-bit ELG-E key, ID 6F7A0B5E, created 2007-01-05
    "Jane Doe <jane@example.net>"
```

### signed only example

```
:compressed packet: algo=1
:onepass_sig packet: keyid 4B0EF9A7289161E2
    version 3, sigclass 0x00, digest 2, pubkey 1, last=1
:literal data packet:
    mode b (62), created 1425242539, name="bashrc_cd_ls",
    raw data: 286 bytes
:signature packet: algo 1, keyid 4B0EF9A7289161E2
    version 4, created 1425242539, md5len 0, sigclass 0x00
    digest algo 2, begin of digest 15 18
    hashed subpkt 2 len 4 (sig created 2015-03-01)
    subpkt 16 len 8 (issuer key ID 4B0EF9A7289161E2)
    data: [2046 bits]
```
