---
title: RAID common operations
date: 2007-03-34
tags:
  - shell
  - raid
---

I never remember the command to manage soft RAID on my servers.

## Create

```shell
mdadm --create --verbose /dev/md0 --level=mirror --raid-devices=2 /dev/sdb1 /dev/sdc1
```

## Status

```shell
cat /proc/mdstat
```

## Replace disk in array

Remove:

```shell
mdadm --manage /dev/md0 --fail /dev/sdb1
mdadm --manage /dev/md0 --remove /dev/sdb1
```

Copy disk setup:

```shell
sfdisk -d /dev/sda | sfdisk /dev/sdb
```

Adding:

```shell
mdadm --zero-superblock /dev/sdb1
mdadm --manage /dev/md0 --add /dev/sdb1
```

## Change raid size

This is to be done in rescue mode.

```shell
# backup
cp /etc/mdadm/mdadm.conf /etc/mdadm/mdadm.conf_orig
mdadm --examine --scan >> /etc/mdadm/mdadm.conf
mdadm -A --scan
```

The resize:

```shell
mdadm --grow /dev/md2 --size=max
resize2fs /dev/md2
e2fsck -f /dev/md2
```
