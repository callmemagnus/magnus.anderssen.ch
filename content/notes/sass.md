---
title: SASS
date: 2017-12-05
---

Install `node-sass`.

## Partials

Filename must start with a `_`. It is imported like this: `@import "partial";`.

Beware using @import pointing to a css file, will not be included at compile time, it will be handled as a "normal" css import.

## Color related mixin

- complement(\$color)
- lighter(\$color, %)
- darker(\$color, %)
- mix($color_a, $color_b, %of_color_a)
- transparentize($color_a, $color_b, %of_what?) // decrease opacity)
- scale_color($color, $red: -35%, $green: -10%, $blue: 0)
- scale_color($color, $saturation: -35%)

## @mixin

Code reuse.

```
@mixin name($parameters: 'default', $option: null) {
    ...
}

@mixin container($parameters: 'default', $option: null) {
    ...
    @content
}

.using { @include name($color); }

.container {
    @include container($parameters: 'toto') {
        // some css
    }
}

@mixin make-transitions($transitions...) { transition: $transitions; }
```

## @extend

Like in a OOP language. It optimizes (which is counter-intuitive but great).

Ex

```
.base {
    color: red;
}
.other {
    @extends .base;
    background-color: yellow;
}
```

Will produce:

```
.base, .other {
    color: red;
}

.other {
    background-color: yellow;
}
```

One can also use placeholder in stead of base class:

```
%base {
    color: red;
}
.other {
    @extends %base;
    background-color: yellow;
}
```

Then, base will not exist in the resulting css.

@extend can be mixed with @mixin.

Note that @extend changes source order.

## @for

```
@for $i from 1 through 10
  .col-#{$i} {
    width: 100% / $i;
  }
}
```

## \@each - loop over lists

### List

```
$items: first, second, last;

@each $item in $items {
  .#{$item} {
    background-image: url('/path/to/#{$item}.png');
  }
}
```

### Map

```
$breakpoints: (sm: 375px, md: 768px, lg: 1200px);

@each $size-name, $bp-value in $breakpoints {
    @media only screen and {min-width: $bp-value} {
        .container-width {
            width: $bp-value;
            &&::before { content: "#{$size-name}"; }
        }
    }
}
```

`map-get` can read any value of a map.

```
$breakpoints: (sm: 375px, md: 768px, lg: 1200px);
$container-widths: (sm: 250px, md: 500px, lg: 750px);

@each $size-name, $bp-value in $breakpoints {
    @media only screen and {min-width: $bp-value} {
        .container-width {
            width: map-get($container-widths, $size-name);
        }
    }
}
```

## @function

Returns a value, @mixins return styles.

```
@function background($color) {
    $variable: stuff;
    @return $variable;
}
```
