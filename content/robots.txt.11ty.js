class Robots {
	data() {
		return {
			permalink: '/robots.txt'
		};
	}

	render(data) {
		const { metadata } = data;

		return `
User-agent: *
Allow: /
Robots: ${metadata.sitemap.url}
      `;
	}
}

module.exports = Robots;
