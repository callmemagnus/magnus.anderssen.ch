---
title: Be prepared for the unexpected
date: 2021-03-13
summary: 'Yes, your data center might burn down'
listed: false
draft: true
notinmix: true
---

## TLDR

As a business owner you need to understand that some of the risks are on your side. If you provide services on top of a server provider, your customers are the ones impacted in case of an issue with the server. Whether the server provider is legally responsible will not save your business if you don't plan thourofully in case of disaster (i.e. have a Disaster Recovery Plan).

## Context

I'm a customer of OVH since several years. I've had a kimsufi server for several years and then moved to their VPS product. I have several VPS for different purposes. One of them is email server and that one, was in their data center located in Strasbourg.

On March 3, I learned that a fire had taken down one of OVH's data centers in Strasbourg; the others on the same premises were off due to lack of electricity.

There is no magic in being prepared. Athletes train all the time, artists rehears. Business owner must be prepared.

The rest of this article will describe my setup.

## Restoring

Restoring a server or service, requires:

- backups
- recent backups
- retrievable backups
- configurations
- another server

I ordered another server, uploaded my data, tweaked and push the configurations to match the new server and the issue was over for me.

### backups

I have a machine at home that regularly backs up all the data on the production servers. It is configured in RAID 1 to reduce the risk of hard drive failure. I chose `rsnapshot` for its simplicity: you define what, the frequency and the retention periods. It might not be the right tool for all use cases but it can be configured to run commands on the server before doing the backup (e.g. perform a db dump).

`rnapshot` just stores the files in your file system.

Regularly, you will need to check for corrupted file but as it relies on rsync, corrupted files get updated with the latest version from the server on the next backup.

Performing a periodic backup, is an issue as you will loose what happened between the last backup and the failure. Another solution would be hot replication. I probably lost some emails as my backup frequency was 4 hours, so I changed it to hourly to reduce the loss.

### configuration

Whether you handle your servers yourself or you delegate that to another service provider, you should always request to obtain the configurations that would enable you to build the server from scratch if needed.

You'll need:

- required packages (dependencies to run your business software)
- product setup
- custom sources --- specific plugins or site theme
-

I finished the configuration of my `ansible` setup to reduce the amount of operations when I add another server to my setup (or have to create a new one because of a failure). I store nearly all the configurations in ansible. The rest are in the backup files and shared as volume to my docker containers.
