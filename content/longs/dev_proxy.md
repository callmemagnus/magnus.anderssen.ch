---
title: dev-proxy or why I fork
date: 2019-03-01
updated: 2019-03-16
summary: |-
  Being the maintainer of a publicly used module is hard. I'm not really one. You get a lot of requests because the module is not solving the specific need of another user. You loose a lot of time sorting issues that might be bugs from requests.
listed: true
tags:
  - development
  - web
  - rant
---

# why I forked dev-proxy

Being the maintainer of a publicly used module is hard. I'm not really one. You get a lot of requests because the module is not solving the specific need of another user. You loose a lot of time sorting issues that might be bugs from requests.

As a module consumer I sometimes get frustrated when the maintainer does not seem to care about my bug reports. I usually take it easy but when I'm blocked by the said bug, I tend to try to either dig into the sources to find the root of the bug or better even propose a fix.

Recently I started using [dev-proxy](https://www.npmjs.com/package/dev-proxy), a simple https to http proxy that was just failing to install on windows.

Quick fix:

```
  ...

  "scripts": {
-    "install": "./bin/create-keys"
+    "install": "node bin/create-keys"
  },

  ...
```

I created the issue and the [PR](https://github.com/azuqua/dev-proxy/pull/3) containing the fix on Jan 16. I was expecting the maintainers to just merge it and release a new minor version.

Since that, zero feedback, no comment, nothing. I added a comment to make the issue "active". But no... Still nothing. I agree that web dev on windows is not mainstream, it's even a bad idea but some, mostly developers in big companies, do not have the choice.

Yesterday, I got bored and created a new version of the same module: [@magnus/dev-proxy](https://www.npmjs.com/package/@magnus/dev-proxy).

So if you need an ssl proxy that simply starts on a port and forwards it locally to another port, use that one if you want it on windows.

Doing this fork brought me to think about how many modules on NPM are just a fix of another module. One might say this is just pollution, other will say it's the beauty of it. This specific problem is not about diversity of features but more about the recognition that maintainer gets from publishing modules. Should maintaining be part of the possibility of publishing? Should we just get used to abandonware on NPM and fork at will? I don't know but I strongly believe that simple PRs should be considered even by busy maintainers. (First world problems first!)
