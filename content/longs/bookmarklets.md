---
title: Bookmarklets
date: 2022-02-11
summary: A praise for bookmarklets
listed: true
tags:
  - development
  - web
---

# Bookmarklets

**I know. I know.** [Bookmarklets](https://en.wikipedia.org/wiki/Bookmarklet) are a _thing of the past_, but no, I don't think so: there are places where they are difficult to replace.

Browser extensions might be more powerful but they come with some security or privacy caveats. More on that at the end.

**Why I'm talking about it ?** Let me present you several use-cases where I use some bookmarklets I wrote.

## Stupidly developed sites

There are lots of site badly developed out there. As a user of `ublock` I struggle sometime to even make a site work. The usual suspect is the integration of tracking on the user path. Some developer finds it to be a good idea to make sure that the tracking is notified before doing the actual command, like the action behind clicking a button. This results in the following in the console:

![window.utag is undefined](../assets/2022-02-11-13-25-03-image.png)

An obviously, the requested operation is not performed.

This should be surrounded with a `try { ... } catch(e) { ... }` statement but someone jumped over some basic front-end classes. More on this dumb attitude in a future post.

The bookmarklet comming to the rescue:

```js
javascript: (function () {
    window.utag = {
        link: () => {}
    };
})();
```

Click on it, and _voilà_, the original action works.

I could also just disable my blocker, but no...

## Creating fake content :-)

I'm lazy.

I like to edit some webpages to create fakes to educate people about how seriously they should take _screenshots_ as proofs for a statement.

![Elon Musk says good bye to earth](../assets/2022-02-11-13-38-27-image.png)

It is true that you can achieve the same by opening the developer console and edit the code but is it lot faster and funnier to just click a button, type and shoot.

The bookmarklet:

```js
javascript: (() => {
    document.body.contentEditable = document.body.contentEditable === 'true' ? false : true;
})();
```

## Jira ticket copier

Software developer gets their work cut into workable pieces (aka tickets). When pushing code, there is usually a constraint on the commit message to contain a relatable ticket. This practice helps others to track the change, know when something is ready and even where it might be available for checks. I voluntarily did not say it is enable understanding of the change in the commit as I think that should be the commit message itself. At best, it might give the context of a change.

Back to the real story, I got bored to select and copy those ticket numbers in Jira. After 10 years working with that tool, I everyday wonder why it is not a feature of the product to copy the ticket id (ex: `ABC-123`) to the clipboard.

So, I wrote something:

```js
javascript: (function () {
    const ticketPattern = /^([A-Z]{1,5}-[0-9]{1,9})$/;

    const ticketElements = Array.from(document.querySelectorAll('*')).filter(element => {
        const ticketPattern = /^([A-Z]{1,5}-[0-9]{1,9})$/;
        return ticketPattern.test(element.innerHTML.trim());
    });

    function removeAll() {
        originalAndWrapperPairs.forEach(({ span, element }) => {
            span.replaceWith(element);
        });
    }

    const originalAndWrapperPairs = ticketElements.map(element => {
        const span = document.createElement('span');

        span.setAttribute(
            'style',
            `
              display: flex;
              gap: 5px;
            `
        );

        element.replaceWith(span);
        span.appendChild(element);

        const button = document.createElement('button');
        button.onclick = event => {
            event.stopPropagation();

            const match = ticketPattern.exec(element.href || element.textContent);

            navigator.clipboard.writeText(match[0]);

            button.innerText = '✔';
            setTimeout(() => {
                removeAll();
            }, 1000);
        };
        button.textContent = '📋';
        button.setAttribute(
            'style',
            `
                background-color: yellow;
                padding: 2px;
                border: none;
                font-size: 1.2rem;
                cursor: pointer;
            `
        );

        span.appendChild(button);
        return {
            span,
            element
        };
    });
})();
```

This snippet of code adds small 📋 besides any ticket numbers it find on the current page.

![Example display of the clipboard icon](../assets/2022-02-12-18-17-21-image.png)

or

![Another example display of the clipboard icon](../assets/2022-02-12-18-18-21-image.png)

Click on one of those clipboard images and you'll see the checkmark to confirm the selection

![Shows a tick when selected](../assets/2022-02-12-18-26-56-image.png)

Then the script cleans up its mess and you have the ticket id in ready to be pasted.

## For the young ones

You are afraid to ask stuff because you have no clue on how to install a _bookmarklet_. Fear not, I will show you how to do it on desktop browser (Firefox)

First, you'll need to have the bookmark toolbar visible. If it's not already the case, press `Ctrl-Alt-b` to toggle the toolbar visibility.

When visible, you can add bookmarklets by:

1. Right click and select `add bookmark...`
2. Give it a name and paste your code starting with `javascript:` in the URL box.
3. Save

Check

- [Firefox bookmarklets](https://support.mozilla.org/en-US/kb/bookmarklets-perform-common-web-page-tasks)
- [Bookmarklets](https://www.chromium.org/user-experience/bookmarklets/)

## Greasemonkey

There are limitation to what you can do with bookmarklets.

First, on every page you need it, you have to click on the bookmarklet. You can inject code but not other libraries depending on the configuration of the site.

For bookmarklets that you use all the time or to add more "stuff", I started exploring that Greasemonkey extensions. You install the extension, give it super powers, and you can do whatever you're able to implement.

I will investigate to add the jira snippet as a greasemonkey script and to create a altassian jira plugin but I'm not sure it's acceptable as it slightly breaks the layout.

See: [Greasemonkey on wikipedia](https://en.wikipedia.org/wiki/Greasemonkey).
