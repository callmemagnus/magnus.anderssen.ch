---
title: How to be blocked from updating your password
summary: |
    Achievement unlocked: being blocked because of non compliant current password
date: 2019-01-29
listed: true
tags:
    - rant
    - web
---

# How to be blocked from updating your password

I was able to get blocked with my current password "quality".

![](./DyEn2SYWwAIdK9i.jpg)

It was not possible to get around the validation in the GUI, so I had to update it through their XHR service

![curl to update password](./DyEq2LfW0AATYlo.jpg)

How would normal user get around this?

Other dangerous password compliance policies applied out there:

![8 to 11 characters](./Dvwix9mWoAI7r3j.jpg)
