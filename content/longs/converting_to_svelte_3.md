---
title: Converting Components to Svelte 3
date: 2019-05-01
notinmix: true
listed: false
draft: true
summary: ...
tags:
  - svelte
  - development
  - web
---

I used svelte 2 on some interactive element of this site. Now that svelte 3 is out, let's see what is needed to migrate.

# Converting Components to Svelte 3

I don't claim all of this is accurate, it seems to work here.

## Changes

### Structure

Before I had things like this in the script part:

```html
<script>
    export default {
        data() {
            return {
                isOpen: false,
                content: ''
            };
        }
    };
</script>
```

In Svelte 2, `isOpen` and `content` are the props of my modules. Component props are now declared like this:

```js
export let isOpen = false;
export let content = '';
```

### Computed values and component initialization

What before was done through the `computed` attributes, needs to happen in the body of the component:

```html
<script>
    export let a = 0;
    export let b = 0;

    let c = a + b;
</script>
```

Fetching data just be done at the same place. If you want the component to work in server-side rendering the fetching needs to happen in the `onMount` method.

```html
<script>
    import { onMount } from 'svelte';

    let items = [];

    onMount(async () => {
        const res = await fetch('host/url');
        items = await res.json();
    });
</script>
```

### Templates and events

TODO

### Manual mounting

This site is not a web app, it is mostly a static site with some enhancements here and there. To initialize my svelte components, I was doing this in my main Javascript file:

```js
import('Component,svelte').then(Component => {
  new Component.default({
    target: element,
-    data: { ... }
+    props: { ... }
  })
})
```

`props` is the new `data`.
