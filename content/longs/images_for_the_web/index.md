---
title: Images on the web
date: 2019-01-29
listed: true
summary: Working at trying to enhance the visits of our customers on the website, we looked into image format and sizes.
tags:
    - web
    - webdev
---

# Images on the web

Working at trying to enhance the visits of our customers on the website, we looked into image format and sizes. I'm also doing a little experiment: _browsing while filtering out images that are bigger than 100kB_. I did so using _ublock_'s `Block media elements larger than` configuration.

The results were not good. Most sites just looked broken without those big images. You might say that some images are useful, I admit that it is the best way to describe a product but there is a trend to add meaning less _happy_ or _bored_ people to just make the site "better".

![waves](./wave.jpg)

The goals I would like anyone to achieve are:

- make sure that the customer or user gets the right amount of information with as few bytes as possible
- while reducing the amount of useless data stored (in cache or on the systems).

Note that _digital_ agencies tend to not be very good at optimizing their delivered assets.

## Choose the correct format for the content

The "technical systems" have no clue on the information to convey from the image and might therefore not optimize it automatically.

Here is a list of frequently seen formats:

- **JPEG** – good for "real-life" images – natural gradients
- **GIF** – don't use
- **PNG** – are better for exported straight edge vector based data, sadly this is the only format supporting correctly transparency.
- **BMP** – no... no... no... just don't use that.
- **WEBP** – not supported in all browsers.

**PNG** and **GIF** have the advantage to support transparency and are usually used only for that capacity instead of the format's feature.

## File extension should match the chosen format

Saving a **PNG** as `whatever.jpg` is bad practice. It brings digital asset management unneeded complexity and it fails to help some systems that relying solely on the extension to act, e.g. web server will set the `content-type` header to the one of the extension.

It is not a huge problem per-se, but it forces every hop to validate the format by trying to open it with different algorithm. This just brings more processing than needed in system and administrators.

## Save for the web

Image to be used on a web site must be prepared for that purpose.

The image must be cleaned from metadata that are just dead payload on the web.

Color spaces must be removed.

### Quality

Depending on the format or encoding algorithm chosen, the file size might be reduce by "degrading" the original image. This is a manual process if one wants to obtain the best ratio.

I recommend [Squoosh](https://squoosh.app/) to visualize the differences.

It is important to keep in mind that the image should be tested in real-life condition and not pinch and zoom.

## More information on images for the web

To dig deeper into the image compression on the web:

- [image.guide](https://images.guide/) -- an e-book by Addy Osmani
- [web.dev#image](https://web.dev/fast/#topic-Optimize-your-images) -- google optimization recommendation, some might not apply here
