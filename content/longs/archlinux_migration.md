---
title: Moving to archlinux (and away from Ubuntu)
summary: Ubuntu is forcing the Snap thing through and I have the feeling that it's not ready yet.
date: 2020-10-04
listed: true
tags:
  - linux
---

# Moving to archlinux (and away from Ubuntu)

The integration of `Snap` pushed me to move away from Ubuntu.

I chose to move to [ArchLinux](https://www.archlinux.org).

The following prose is my installation history and the stuff that I had to do outside of normal actions of the tools in order to have working machines. I decided to use [ZEN installer](https://sourceforge.net/projects/revenge-installer/) to do the installation because I'm lazy (comes at a cost).

I installed 2 machines lately:

1. Dell XPS 13 (9360)
2. Zotac MI 660 nano

/!\\ Remember to perform a backup before doing anything (or buy another disk and an enclosing).

## Not enough space

I had an issue when installing using Zen installer. I went through all the sections, it starts the installation by downloading packages to just fail: telling me that it could not finish. I sadly don't remember the exact error.

I started changing my answers to Zen's questions to try to find if and what was breaking the install, thinking that there was a dependency issue. Then I saw that `pacstrap` failed in the same manner.

It seems that the issue lays (laid) on the mirror selection process. I limited the mirror selection to one country and it worked. I was able to finish the installation on the first machine. The second machine had the same issue.

## Yet another yogurt

[AUR](https://aur.archlinux.org/) is Archlinux user repository where some users publish packages to be used on Archlinux. Sometimes they are bleeding edge version of existing packages, some obscure software or simply blobs: my printer and some stupid software (e.g. Skype) require proprietary binaries that are made available through AUR, thanks to all those contributing users.

To handle these packages I choose to install [yet another yogurt](https://github.com/Jguer/yay). Installing `yay` enables simple access to AUR and serves as a `pacman` frontend.

You can then install any package with `yay`.

## Wifi

For some reason, the wifi on my laptop worked out of the box with the `wifi-menu` tool, but on the Zotac it didn't. I had to start it manually with the following commands (See [ArchLinux - Wireless configuration](https://wiki.archlinux.org/index.php/Network_configuration/Wireless) & [ArchLinux - WPA](https://wiki.archlinux.org/index.php/Wpa_supplicant)).

Just to know what is inside the Zotac desktop:

```
$ lspci -k
---8<---
01:00.0 Network controller: Intel Corporation Wireless 3165 (rev 81)
        Subsystem: Intel Corporation Dual Band Wireless AC 3165
        Kernel driver in use: iwlwifi
        Kernel modules: iwlwifi
---8<---
```

I used the following commands in order to activate and configure the link and wifi network:

```
# ip link set wlan0 up
# wpa_supplicant -B -i wlan0 -c <(wpa_passphrase MYSSID passphrase)
# dhclient
```

After some seconds, it's connected! We can continue the installation process.

## AER

Don't ask me what that is, but it generates a lot of logs on my Zotac. So much that it makes Zen installer unusable. It seems related to the wifi. This issue was also present in Ubuntu 20.04.

To remove those annoying logs, `pci=noaer` must be added to the grub start line. On GRUB (or whatever it is on the .iso) startup screen, type `e` and add it to the end of the line. Remember that the system is using a US keymap at that moment.

After installation, if the error logs are still present, you'll need to update grub (or any other boot manager) to add the same attribute to the normal boot sequence. For grub, first edit `/etc/default/grub` to add it to the `GRUB_CMDLINE_LINUX_DEFAULT`, then run:

```
sudo grub-mkconfig -o /boot/grub/grub.cfg
```

And finally reboot to enjoy!

## Halt

`halt` "now" halts instead of poweroff. [See this message](https://bbs.archlinux.org/viewtopic.php?id=152610).

## Post install configurations

The following notes are mostly around thing that I change on Gnome 3.

### gnome-shell extensions

I use gnome.

I prefer to install extensions from AUR. Only one is **not** available there: `alternativeTab`, which gets rid of the useless alt-tab gnome has as default (maybe I can solve this another way).

The others

- `gnome-shell-extension-gsconnect-git`: enabling interaction with kconnect app on an Android phone
- `gnome-shell-extension-pixel-saver-git`: remove window title bar when full or half screen
- `gnome-shell-extension-stealmyfocus`: removes the "window is ready"
- `gnome-shell-extension-topicons-plus-git`: those small application icons of background applications (e.g. Nextcloud, Skype, Flameshot)

Don't forget to enable them after installation!

### printscreen tool

I use [Flameshot](https://flameshot.js.org/), installed with pacman.

To integrate it, the out of the box `printscreen` button shortcut assignment must be removed:

```
$ gsettings set org.gnome.settings-daemon.plugins.media-keys screenshot []
```

Then to create a new custom shortcut, add `flameshot gui` as the command and assign the `printscreen` key as shortcut.
