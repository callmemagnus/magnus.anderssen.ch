---
title: Open letter to PostFinance
date: 2019-09-01
updated: 2019-12-13
summary: 'Write-up about my frustration as being your customer.'
listed: true
tags:
  - android
  - rant
---

**UPDATE 4**: I finally got a communication from the "customer reaction" department.

> PostFinance ne souhaite pas créer deux applications distinctes. La sécurité et la protection des données est un élément primordial pour la majorité de notre clientèle. Pour cette raison, PostFinance ne prend plus en charge les appareils rootés ou débridés.
> 
> Nous regrettons de ne pouvoir vous satisfaire dans cette affaire et vous remercions de votre compréhension.

In substance, it is a fsck you. She writes that for the majority (translate: old unpatched phones) data security is important. She make the shortcut of a technical decision (the current implementation) is being wished by the most of the customer base, instead of just moving that decision to the user, like some other banks do. They are bad at product management and don't know how to fix their app.

I give up!

**UPDATE 3**: Version 4.9.0 was released but the application is still crashing on start, error in the logs is exactly the same than 4.8.0. The grade in PlayStore is now 2.9 and there is a majority of 1 star ratings. Somebody has an alternative bank to propose?

**UPDATE 2**: After several message exchange with Postfinance people, I got a new deadline: November 2019. Let's wait and see.

**UPDATE 1**: There seems to be non-rooted phones having the same-ish issue. This is then becoming very interesting.

I have been a customer of all the shapes of PostFinance in the last 25 years. My dad and I open my account when I was a teenager in order for me "to learn to manage the little money I had back then".
I was a first time user when yellownet appeared and already then thought it was the future of banking. I've always accommodated the new fees systems regularly introduced to be able to never pay any of those fees.

Since the beginning of my career, all my paycheck have been sent to that bank.

Until recently.

Sadly this relationship became shaky (Facebook users would maybe tag it as "it's complicated"). I have become an addict to your mobile application; from which, I expect 3 things:

1. be able to quickly see my balance
2. be able to scan those infamous "BVR" in order to not have to type them
3. fast to open (e.g. fingerprint)

# Open letter to Postfinance

## The fingerprint sensor on Android 6

It first started 2 or 3 years ago with one of your dependency upgrade which dropped support for Android 6, I was able to get that information after several back and forth with your support team which finally told me that my phone was obsolete. After that upgrade, I lost the fingerprint login, although ALL other applications using the fingerprint sensor continued to work. I was told to upgrade my phone. Right. When you know the lapse of time major manufacturers do it, what could I expect from mine...

I tasted the **phone support**, which was on first level near to useless, telling me among other stupid answers, that I had dreamed the feature. So, no more phone calls to your support, I have better things to do. You should definitely train your first line more on the products and on basic communication skill (I'm not talking about these polite kind empty answering practices but to enable them to communicate more efficiently and bail out when THEY HAVE NO CLUE about the topic).

Luckily for you, that phone hit the floor one time too many and it was dead. So I had to get a newer one. I chose Galaxy S7 on which, obviously I installed LineageOS because I want to have more control on my phone, on my privacy and be sure to get security upgrades. Your app had nothing to say against my new phone so our journey continued with a seamless experience but still there was a bad taste in my mouth.

Then came the infamous version 4.8, the one that would bring me whatever (extensive features, see below) I won't really need (remember my 3 use-cases) and that will just bloat the app yet a little bit more.

On my fresh upgraded firmware with the latest security patches, the app just refused to start and crashed miserably without any last words.

## Reporting failure (again)

I immediately looked into the logs took what was the source of the problem and sent a report to the official email (postfinance@postfinance.ch) :

> Hi,
> 
> I upgraded your android app this morning. It now crashes on start.
> 
> I have this in the logs:
> 
> 06-12 07:35:47.817 13282 13282 D AndroidRuntime: Shutting down VM
> 06-12 07:35:47.817 13282 13282 E AndroidRuntime: FATAL EXCEPTION: main
> 06-12 07:35:47.817 13282 13282 E AndroidRuntime: Process: ch.postfinance.android, PID: 13282
> 06-12 07:35:47.817 13282 13282 E AndroidRuntime: java.lang.UnsatisfiedLinkError: JNI_ERR returned from JNI_OnLoad in "/data/app/ch.postfinance.android-MeJUvE5lAfDiFhD3PVCK2g==/lib/arm/libmfjava.so"
> 06-12 07:35:47.817 13282 13282 E AndroidRuntime: at java.lang.Runtime.loadLibrary0(Runtime.java:1016)
> 06-12 07:35:47.817 13282 13282 E AndroidRuntime: at java.lang.System.loadLibrary(System.java:1669)
> 06-12 07:35:47.817 13282 13282 E AndroidRuntime: at ch.postfinance.core.CurrentActivityHandlingApp.<clinit>(Unknown Source:2)
> 06-12 07:35:47.817 13282 13282 E AndroidRuntime: at java.lang.Class.newInstance(Native Method)
> 06-12 07:35:47.817 13282 13282 E AndroidRuntime: at android.app.AppComponentFactory.instantiateApplication(AppComponentFactory.java:50)
> 06-12 07:35:47.817 13282 13282 E AndroidRuntime: at android.app.Instrumentation.newApplication(Instrumentation.java:1120)
> 06-12 07:35:47.817 13282 13282 E AndroidRuntime: at android.app.LoadedApk.makeApplication(LoadedApk.java:1061)
> 06-12 07:35:47.817 13282 13282 E AndroidRuntime: at android.app.ActivityThread.handleBindApplication(ActivityThread.java:5891)
> 06-12 07:35:47.817 13282 13282 E AndroidRuntime: at android.app.ActivityThread.access$1100(ActivityThread.java:200)
> 06-12 07:35:47.817 13282 13282 E AndroidRuntime:     at android.app.ActivityThread$H.handleMessage(ActivityThread.java:1656)
> 06-12 07:35:47.817 13282 13282 E AndroidRuntime: at android.os.Handler.dispatchMessage(Handler.java:106)
> 06-12 07:35:47.817 13282 13282 E AndroidRuntime: at android.os.Looper.loop(Looper.java:193)
> 06-12 07:35:47.817 13282 13282 E AndroidRuntime: at android.app.ActivityThread.main(ActivityThread.java:6718)
> 06-12 07:35:47.817 13282 13282 E AndroidRuntime: at java.lang.reflect.Method.invoke(Native Method)
> 06-12 07:35:47.817 13282 13282 E AndroidRuntime: at com.android.internal.os.RuntimeInit\$MethodAndArgsCaller.run(RuntimeInit.java:493)
> 06-12 07:35:47.817 13282 13282 E AndroidRuntime: at com.android.internal.os.ZygoteInit.main(ZygoteInit.java:858)
> 06-12 07:35:47.822 3682 5605 W ActivityManager: Force finishing activity ch.postfinance.android/.ui.home.SplashScreenActivity
> 
> I tried to uninstall and re-install the application without any more success.
> 
> Regards,
> 
> Magnus Anderssen

To which I received a thank(fuck) you reply with a ticket number (13269817).

I expect that a human take those report quickly, users cannot use the product so the same day I started it on twitter too, as sometimes those representative have more knowledge on digital products:

<blockquote class="twitter-tweet" data-dnt="true"><p lang="en" dir="ltr">Hey <a href="https://twitter.com/PostFinance?ref_src=twsrc%5Etfw">@PostFinance</a>, can you fix your android app? Seems that a lot of users have issues with the update.<br><br>And no, we don&#39;t want to call your bad hotline.</p>— Magnus (@m_gn_s) <a href="https://twitter.com/m_gn_s/status/1138730075204050946?ref_src=twsrc%5Etfw">June 12, 2019</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

I got some reply to take it in DM, where the operator wasn't able to link my response to the original thread so again... Useless. You should definitely use a better "social media" tool.

3 days later I tried again:

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">Still no news from <a href="https://twitter.com/PostFinance?ref_src=twsrc%5Etfw">@PostFinance</a>.<br><br>Provided error logs by email, no answer.<br>Provided crash report, no answer.<br>Provided phone model by DM, got asked how they could help...<br><br>Communication is an art, make sure the people you put in the trenches master it!<br><br>Quite disappointed... <a href="https://t.co/ER8q2HO2de">https://t.co/ER8q2HO2de</a></p>— Magnus (@m_gn_s) <a href="https://twitter.com/m_gn_s/status/1139789127275175936?ref_src=twsrc%5Etfw">June 15, 2019</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

No answers.

I added a little bit more to the menu:

<blockquote class="twitter-tweet" data-dnt="true"><p lang="en" dir="ltr">6 days without communication. When I see the reviews (<a href="https://t.co/rc5Wp4ueVU">https://t.co/rc5Wp4ueVU</a>), I&#39;m not the only one having issues with the latest version of the android app.<br><br>Helloooooo, anybody at <a href="https://twitter.com/PostFinance?ref_src=twsrc%5Etfw">@PostFinance</a> wants to give some feedback? <a href="https://t.co/TLXxEDUWUt">https://t.co/TLXxEDUWUt</a></p>— Magnus (@m_gn_s) <a href="https://twitter.com/m_gn_s/status/1140875152634372096?ref_src=twsrc%5Etfw">June 18, 2019</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

In that last tweet I mentioned the Play Store ranking which was going down.

## Play Store

On a side note, the bot that replies to every bad review that the author should call them is an anomaly. The consultant who sold that idea to you is a pervert. If people are writing reviews or sending you debugging mails, it is to not have to get through the first line of the "Contact center".

The grade of the app is now **3.3**, it was more than 4 before this upgrade. I feel less lonely now.

## Your responses

I finally got an answer to my original report 8 (eight) days later. After the excitement of being your valuable user faded away, I saw that it was just a request for more information.

The email explain that they have limited testing capacity (by choice) and that they can't support every phone.

> You informed us that our newest App keeps crashing and not opening.
> 
> We thank you for your logs and sincerely apologize for this late answer. At the moment, some clients are encountering the > same issue upon the opening of the App and our specialists are trying to find a solution as quickly as possible.
> 
> We would like to inform you that our developers have tested only devices that have the original standards. If you have made > any changes to your phone by rooting or jailbreaking it, PostFinance cannot guarantee that our App works for such changes.
> 
> In order to resolve this technical problem as fast as possible, can you please send us the following information:
> 
> - Which mobile phone and model do you have? (Samsung Galaxy s9, SM-...)
> - Which operating system does your phone have? (Android, iOS...)
> - Did you receive an error message? (If so, which one?)
> - Can you please send us a print screen of the sudden closure error?
> - Does this error also occur with Wlan and 4G connection?

Back to square one. I definitely would have preferred a dev to request for a call, but no, they prefer to send a trained intermediate who has few clues on what the whole thing is about.

I stay convinced that if your application is using the Android SDK and trusts the sandbox given by the operating system, the only reason why you should test is for the information layout on the screen and the different Android version compatibility.

## For security reasons

I had a slow but promising exchange until this bomb hit in my mailbox:

> After analyzing your data, we regret to inform you that for security reasons, the new PostFinance App version (4.8.0) is not available for rooted devices. As an alternative to the PostFinance App, you can log into your e-finance access within your Internet browser (www.postfinance.ch), either with your yellow card reader and your PostFinance Card, or with Mobile ID.
> 
> As we develop the functionality of our PostFinance App, we are adapting our security requirements for smartphones. Due to the extensive functionality in the new version of the application, security requirements have been strengthened. As a result, our app no ​​longer works on rooted smartphones.

**So the reason is security**. My rooted phone has the latest security patches available and the app won't run but a friend's Stock Galaxy S4 with no security update since 2016 can run the app? This is nonsense.

First, I never stated my phone was rooted. Second, lineageOS comes **not rooted** by default, showing again the lack of knowledge around after-market ROMs. Everybody thinks things should be like that Apple ecosystem: outside is bad. I wonder how long you will take to tell your customer that they have to use a specific browser or OS "for security reasons". This is just dumb (or badly informed) product management.

And again, back to my 3 use-cases, I'm not sure your "extensive functionality" is an excuse to exclude me from the application.

## My 2 cents for you

I have the following advices for your company specific job titles (or whatever it is called in PostFinance)

### CISO

As the CISO, you must know that security is being used as an excuse to not provide a service to a customer. If this is understood and correctly assessed (obviously I would not agree on that) it is "fine" (for PostFinance). If not, I would definitely liaise with the mobile application product owner to sort this out.

I still wonder what kind of security requirement lead the product team to implement such a bad solution. I tested several different banking mobile app; either they don't care about root or my ROM or they restrict some features. Some just don't work if they detect root but that is not really a problem and work correctly after some adjustments.

### Contact center manager

Your people are the first line of contact for desperate customers, therefore your people must be trained accordingly. As I wrote before, I don't mind not getting "I'm sorry" talks if I can get correct answers. Your people must also know they don't know; my previous issue with the application showed me that some had zero knowledge of the products available for your customer. If that is the case, I don't mind being sent to someone else as long as I don't have to wait.

I don't know if the people answering to the internal mailbox (through the ebanking portal) are the same but they suffer the same disease. Following this incident, I sent a message stating that if I cannot have my 3 use-cases by the end of January 2020, I will close my account. Guess what the answer was... I got an guide on how to register to the fast service in the application. I frankly don't understand how someone could read so little of my original message.

### Mobile application product management

You failed. Probably because of lack of knowledge and lack of counselling. Could you have avoided this failure? Probably. If you had had sufficient information and understanding of the mobile phone ecosystem.

Being told that I have to wait for the next release and hope/pray that it will work is not acceptable. You definitely know if and when you plan to fix this. It will not be an accident if this gets resolved, it has to be on the roadmap.

As a customer, I expect more from you! Transparency would be a nice start. Yes, you told me that you won't support rooted phones but this looks more like an accident on the path of adding new features...

You also need to state this on your website. As I said before, a working mobile application is mandatory for a lot of your customers, therefore the growing part of the population who is using a rooted phone wants to know this UPFRONT. And don't burry that information in your usage conditions, this would be unfair.

### Mobile application developer

You didn't help here. You are probably the most knowledgeable of all in this story. Except if the failing part arrived by integrating a third party crap, you know how a simple-stupid app should be developed.

Part of your role is to help other stakeholders like your product managers to make the correct decisions. I know there are constraints but it is needed to move the boat to a better path. Frankly this no-go on rooted devices is dumb. Play store reviews show that I'm not an isolated type of user.

Make your voice be heard!

## Conclusion

The outcome of this is that I can't have an app for my 3 use-cases.

You seem to have chosen your customer base and it does not include me.
