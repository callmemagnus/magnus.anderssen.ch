const SPECIAL_PAGES = ["/", "/mix/"];

const urlEntry = (loc, lastmod, changefreq = "monthly", priority = 0.8) => `
  <url>
    <loc>${loc}</loc>
    <lastmod>${new Date(lastmod).toISOString()}</lastmod>
    <changefreq>${changefreq}</changefreq>
    <priority>${priority}</priority>
  </url>
  `;

const absoluteUrl = (base) => (pathname) => new URL(pathname, base).toString();

const renderSpecialPages = (collections, abs) => {
  return SPECIAL_PAGES.reduce((acc, urlToFind) => {
    const page = collections.all.find(({ url }) => urlToFind === url);
    return (acc += urlEntry(abs(page.url), page.date, "weekly", 1));
  }, "");
};

const renderListed = (collections, abs) => {
  return collections.mix.reduce(
    (acc, item) => (acc += urlEntry(abs(item.url), item.date)),
    ""
  );
};

class Sitemap {
  data() {
    return {
      permalink: "/sitemap.xml",
    };
  }

  render(data) {
    const { metadata, collections } = data;

    console.log(metadata);

    return `<?xml version="1.0" encoding="UTF-8"?>
      <urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
        ${renderSpecialPages(collections, absoluteUrl(metadata.url))}
        ${renderListed(collections, absoluteUrl(metadata.url))}
      </urlset>
    `;
  }
}

module.exports = Sitemap;
