---
title: Web screen split library
url: https://split.js.org/
date: 2019-02-21
listed: true
tags:
  - development
  - web
---

Small library to enable space splitting in a web interface.

Might be useful for internal applications or development oriented content.
