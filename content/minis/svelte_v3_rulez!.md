---
title: Svelte v3 rulez!
date: 2019-08-28
url: https://svelte.dev/tutorial/basics
summary: 'Svelte is the "new" kid on the block and it is great!'
listed: true
tags:
  - development
  - web
  - svelte
---

Svelte is great, not because it is new but because it solves different problems!

Among those:

- bundle sizes might be rather small
- simplicity to get started
- isolation
- ...

Check out by yourselves: [svelte homepage](https://svelte.dev)!

Or [start the tutorial](https://svelte.dev/tutorial/basics)!
