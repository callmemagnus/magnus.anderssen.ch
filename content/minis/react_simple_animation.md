---
title: React simple animation
date: 2019-06-15
url: https://react-simple-animate.now.sh/
listed: true
tags:
  - react
  - development
  - web
---

Very interesting package to look into to do some animations with React.
