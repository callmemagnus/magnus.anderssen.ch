---
title: Formik for React forms
date: 2019-04-28
url: https://jaredpalmer.com/formik/
summary: |
  Formik is a tool to handle all the boilerplate code around forms in React.
listed: true
tags:
  - react
  - development
  - web
---

Formik is a tool to handle all the boilerplate code around forms in React.

It solved most of the headaches that form building can bring.

Explained by its author [@jaredpalmer](https://twitter.com/jaredpalmer):

::: youtube oiNtnehlaTo :::

What I like about it:

- abstracts away most of the form development complexity
- opt-in
- uses [render props](https://reactjs.org/docs/render-props.html) to give full render control
- removes the "need" to include forms in the state management
- possible to include existing widgets into forms

Go check yourself.
