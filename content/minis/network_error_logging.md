---
title: Network Error Logging
date: 2020-10-29
url: https://medium.com/@PeterPerlepes/server-responses-so-slow-the-user-abandoned-trace-using-nel-and-an-example-in-node-js-b83cf5719499?s=08
listed: true
tags:
  - development
  - web
---

Server responses so slow the user abandoned! Trace using NEL and an example in Node.js
