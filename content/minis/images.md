---
title: ebook on images on the web
url: https://images.guide/
date: 2019-02-01
listed: true
tags:
  - web
  - development
---

Just about everything you need to know about images.

And be sure to use [squoosh.app](https://squoosh.app/) to test.
