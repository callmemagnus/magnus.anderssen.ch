---
title: frontend checklist
url: https://frontendchecklist.io/
date: 2018-11-29
listed: true
tags:
  - development
  - web
---

Nice tool to check before publishing a new web site.
