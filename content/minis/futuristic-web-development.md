---
title: Futuristic Web development
url: https://www.youtube.com/watch?v=qSfdtmcZ4d0
date: 2020-10-28
listed: true
tags:
  - development
  - web
  - sveltekit
  - svelte
---

Watch this potential future replacement of svelte/Sapper.
