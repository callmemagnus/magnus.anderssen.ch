---
title: Uppy file uploader
date: 2019-02-17
url: https://uppy.io/
listed: true
tags:
  - development
  - web
---

Is this the new "best in class" for file uploader?

It supports lots of "uploaders", configurable XHR, S3...

File can come from webcam, drag-n-drop or from "providers" like google-drive, instagram.
