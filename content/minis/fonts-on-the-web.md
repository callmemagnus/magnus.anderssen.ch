---
title: Handling fonts on the web
url: https://www.filamentgroup.com/lab/js-web-fonts.html
date: 2018-10-30
listed: true
tags:
  - development
  - web
---

The most concise guide on how to load fonts on the web.
