---
title: My linux doesn't boot anymore or bad kernel upgrade ?
date: 2022-07-08
summary: '...cannot find initramfs...'
listed: true
notinmix: false
tags:
  - linux
  - archlinux
---

symptoms: cannot boot, cannot find initramfs.

I have no clue on how it happened. Maybe the upgrade process was interupted or I failed to reboot after a kernel upgrade and did (several) other kernel upgrades... I'll never know, but it can be fixed!

Let's fix the damn laptop. This worked on an Archlinux installation.

## Prepare USB boot disk

If not already at hand

1. Have a USB key at hand
2. Download linux ISO
3. "Burn" the ISO on the key (e.g. gnome disk restore feature)

## Rebuild the boot files

Boot from the USB disk, at the prompt:

```shell
mount /dev/whatever /mnt   # / (root)
mount /dev/else /mnt/boot  # /boot
```

All needed partitions must be mounted before the next step. Usually there is one (/), two (/ and /boot) or three (/ and /boot and /boot/efi)

Then chroot,

```shell
arch-chroot /mnt
```

Hoping that the packages are up to date and that the problem is not related to a package problem, continue by rebuilding the cpio and grub configuration.

```shell
mkinitcpio -p linux # or linux-lts or linux-zen
grub-mkconfig -o /boot/grub/grub.cfg
```

## Cross fingers

Quit the chroot (Ctrl-D) and reboot.
