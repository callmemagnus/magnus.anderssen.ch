---
title: Digital Psychology
date: 2019-04-24
url: https://digitalpsychology.io/
listed: true
tags:
  - links
---

Very interesting!

Digital Psychology: a free library of psychological principles and examples for inspiration to enhance the customer experience and connect with your users.
