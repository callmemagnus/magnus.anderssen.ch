---
title: Svelte summit 2021
summary: |
  This year's svelte summit was great.
  My picks of this year's summit in no particular order.
date: 2021-05-01
listed: true
tags:
  - svelte
  - development
  - web
---

This year's svelte summit was great

My picks of this year's summit in no particular order:

## Zwoosh YEAHHH Shabam! 🤯 Whimsy in motion

funny talk with lots of good ideas to have some fun while developing

::: youtube fnr9XWvjJHw?start=17754 :::

## Reactive Forms

Interesting approach to handle forms

::: youtube fnr9XWvjJHw?t=15454 :::

## Svelte Can Compile and so can you!

precompiled i18n lib (svelte-intl-precompile)

::: youtube fnr9XWvjJHw?t=10008 :::

## An update on SvelteKit

Rich Harris as usual! :-)

::: youtube fnr9XWvjJHw?t=19101s :::
