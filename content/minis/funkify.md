---
title: Disabilities simulation on the web
url: https://funkify.org
date: 2018-12-07
listed: true
tags:
  - web
---

Funkify is a chrome extension to simulate different disabilities your user might encounter.
