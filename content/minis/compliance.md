---
title: Compliance is the key
date: 2019-02-13
listed: true
tags:
  - rant
---

**Rule**: They say that all electronic equipment must be secured.

**Implementation**:
![loading-ag-1460](../assets/compliance.jpg)
