---
title: CLI file manager like VIM
url: https://ranger.github.io/
date: 2019-02-06
listed: true
tags:
  - cli
  - linux
---

Just because we can!
