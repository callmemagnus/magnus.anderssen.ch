const fs = require("fs");
const path = require("path");

const filepath = path.resolve(__dirname, "../../dist/manifest.json");
console.log("manifest data");

module.exports = function () {
  let interval;
  const check = (resolve) => {
    if (fs.existsSync(filepath)) {
      clearInterval(interval);
      resolve(JSON.parse(fs.readFileSync(filepath).toString()));
    }
  };

  return new Promise((resolve) => {
    check(resolve);
    interval = setInterval(() => check(resolve), 100);
  });
};
