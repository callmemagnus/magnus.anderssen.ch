module.exports = {
    url: "https://magnus.anderssen.ch",
    author: "Magnus Anderssen",
    feed: {
        url: "https://magnus.anderssen.ch/feed.xml"
    },
    sitemap: {
        url: "https://magnus.anderssen.ch/sitemap.xml"
    }
};
