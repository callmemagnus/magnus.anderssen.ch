const Nunjucks = require('nunjucks');
const syntaxHighlight = require('@11ty/eleventy-plugin-syntaxhighlight');
const directoryOutputPlugin = require('@11ty/eleventy-plugin-directory-output');

const absoluteUrl = require('./_11ty/filters/absoluteUrl');
const addHash = require('./_11ty/filters/addHash');
const dateToDay = require('./_11ty/filters/dateToDay');
const truncateUrl = require('./_11ty/filters/truncateUrl');
const markdown = require('./_11ty/markdown');

const removeDraft = item =>
	process.env.EDITING === 'true' || (typeof item.data.draft !== 'undefined' ? !item.data.draft : true);
const removeFromMix = item => !item.data.notinmix && removeDraft(item);
const removeUnlisted = item =>
	process.env.EDITING === 'true' || (typeof item.data.listed !== 'undefined' ? !!item.data.listed : true);
const removeNotes = item => {
	return !/\/notes\//.test(item.url);
};

const now = Date.now();

const sortByUpdated = (itemA, itemB) => {
	const criteriaA = itemA.data.updated ? itemA.data.updated : itemA.date;
	const criteriaB = itemB.data.updated ? itemB.data.updated : itemB.date;

	if (criteriaA < criteriaB) {
		return 1;
	} else if (criteriaA === criteriaB) {
		return 0;
	} else {
		return -1;
	}
};

console.log('EDITING is ', process.env.EDITING);

module.exports = function (eleventyConfig) {
	eleventyConfig.addGlobalData('now', new Date(now).toISOString());

	// mardown template engine breaks permalinks
	eleventyConfig.setQuietMode(true);
	if (process.env.NODE_ENV === 'production') {
		eleventyConfig.addPlugin(directoryOutputPlugin);
	}

	eleventyConfig.setDynamicPermalinks(true);
	eleventyConfig.addPlugin(syntaxHighlight, {
		templateFormats: ['md']
	});

	eleventyConfig.addPassthroughCopy('static');
	eleventyConfig.addPassthroughCopy('_redirects');
	eleventyConfig.addPassthroughCopy('_location.part.conf');

	let nunjucksEnvironment = new Nunjucks.Environment(new Nunjucks.FileSystemLoader('templates'), {
		watch: process.env.NODE_ENV !== 'production',
		noCache: process.env.NODE_ENV === 'production'
	});

	nunjucksEnvironment.addFilter('dateToDay', dateToDay);
	nunjucksEnvironment.addFilter('absoluteUrl', absoluteUrl);
	nunjucksEnvironment.addFilter('truncateUrl', truncateUrl);
	nunjucksEnvironment.addFilter('addHash', addHash, true);

	eleventyConfig.setLibrary('njk', nunjucksEnvironment);

	eleventyConfig.addCollection('mix', collection =>
		collection
			.getAllSorted()
			.filter(removeNotes)
			.filter(removeUnlisted)
			.filter(removeFromMix)
			// remove list pages
			.filter(({ url }) => url.split('/').length > 3)
			.sort(sortByUpdated)
	);

	function filterTagList(posts) {
		return (posts || []).filter(removeNotes).filter(removeUnlisted).filter(removeFromMix);
	}

	eleventyConfig.addFilter('filterTagList', filterTagList);

	// Create an array of all tags
	eleventyConfig.addCollection('tagList', function (collection) {
		const tagSet = new Set();
		const tagCounter = {};
		collection
			.getAll()
			.filter(removeNotes)
			.filter(removeUnlisted)
			.filter(removeFromMix)
			.forEach(item => {
				(item.data.tags || []).forEach(tag => {
					if (!tagSet.has(tag)) {
						tagCounter[tag] = 0;
					}
					tagCounter[tag]++;
					tagSet.add(tag);
				});
			});

		const result = Array.from(tagSet).map(tag => ({
			count: tagCounter[tag],
			tag
		}));
		result.sort((a, b) => b.count - a.count);
		return result;
	});

	['notes', 'longs', 'minis'].forEach(type => {
		eleventyConfig.addCollection(type, collection =>
			collection
				.getFilteredByGlob(`content/${type}/**/*.md`)
				.filter(removeDraft)
				.filter(removeUnlisted)
				.sort(sortByUpdated)
		);
	});

	eleventyConfig.setLibrary('md', markdown);
	// eleventyConfig.setWatchThrottleWaitTime(500); // in milliseconds

	return {
		templateFormats: ['md', 'njk', '11ty.js', 'jpg', 'html'],
		dir: {
			input: 'content',
			output: 'dist',
			data: 'data',
			includes: '../templates'
		},
		// disabled njk processing of {{ xxx }}
		markdownTemplateEngine: 'md',
		dataTemplateEngine: '11ty.js',
		passthroughFileCopy: true
	};
};
