const markdownIt = require('markdown-it');
const markdownContainer = require('markdown-it-container');
const path = require('path');
const Image = require('@11ty/eleventy-img');

let options = {
	html: true
};

const md = markdownIt(options)
	// from https://github.com/markdown-it/markdown-it-container
	// This is not accurate as we fake the container by "ignoring" the
	// end of the container
	.use(markdownContainer, 'youtube', {
		validate(params) {
			return params.trim().match(/^youtube\s+(.*)/);
		},
		render(tokens, idx) {
			if (tokens[idx].nesting === 1) {
				const m = tokens[idx].info.trim().match(/^youtube\s+(.*)$/);
				// opening tag
				return `
<div class="youtube" style="
  position: relative;
  width: 100%;
  height: 0;
  padding-bottom: 56.25%;
  margin-bottom: 1em"
>
<iframe
  style="
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
  "
  src="https://www.youtube-nocookie.com/embed/${m[1]}"
  frameborder="0"
  allow="accelerometer; encrypted-media; gyroscope; picture-in-picture"
  allowfullscreen
></iframe></div>
        `;
			}
			return '';
		}
	});

md.renderer.rules.image = function (tokens, idx, options, env, slf) {
	var token = tokens[idx];
	token.attrs[token.attrIndex('alt')][1] = slf.renderInlineAsText(token.children, options, env);

	/*
Token {
  type: 'image',
  tag: 'img',
  attrs: [ [ 'src', './farmy_01.png' ], [ 'alt', 'Page arrival' ] ],
  map: null,
  nesting: 0,
  level: 0,
  children: [
    Token {
      type: 'text',
      tag: '',
      attrs: null,
      map: null,
      nesting: 0,
      level: 0,
      children: null,
      content: 'Page arrival',
      markup: '',
      info: '',
      meta: null,
      block: false,
      hidden: false
    }
  ],
  content: 'Page arrival',
  markup: '',
  info: '',
  meta: null,
  block: false,
  hidden: false
}
*/

	// console.log(env.page);
	/*
{
  date: 2022-01-26T00:00:00.000Z,
  inputPath: './content/longs/critical-resources/index.md',
  fileSlug: 'critical-resources',
  filePathStem: '/longs/critical-resources/index',
  outputFileExtension: 'html',
  url: '/longs/critical-resources/',
  outputPath: 'dist/longs/critical-resources/index.html'
}
*/

	const imgRelativePath = token.attrs[token.attrIndex('src')][1];
	let imagePath;

	const urlPath = env.page.url;

	// only care about local images
	const pathToHere = path.dirname(env.page.inputPath);
	const pathToDist = path.resolve(path.dirname(env.page.outputPath));

	imagePath = path.resolve(pathToHere, imgRelativePath);

	const imgOptions = {
		widths: [730, null], // null to keep original
		formats: ['webp'],
		urlPath: urlPath,
		outputDir: pathToDist,
		filenameFormat: (id, src, width, format, options) => {
			const extension = path.extname(imagePath);
			const name = path.basename(imagePath, extension);

			return `${name}-${width}w.${format}`;
		}
	};

	console.log(`🚀 Generating image ${path.basename(imgRelativePath)}`);
	try {
		Image(imagePath, imgOptions);
		let metadata = Image.statsSync(imagePath, imgOptions);
		// console.log(metadata);

		/*
{
  webp: [
    {
      format: 'webp',
      width: 300,
      height: 117,
      url: '/longs/critical-resources/pyV29OSBTE-300.webp',
      sourceType: 'image/webp',
      srcset: '/longs/critical-resources/pyV29OSBTE-300.webp 300w',
      filename: 'pyV29OSBTE-300.webp',
      outputPath: '/home/magnus/cloud/src/sites/magnus.anderssen.ch/dist/longs/critical-resources/pyV29OSBTE-300.webp'
    },
    {
      format: 'webp',
      width: 600,
      height: 235,
      url: '/longs/critical-resources/pyV29OSBTE-600.webp',
      sourceType: 'image/webp',
      srcset: '/longs/critical-resources/pyV29OSBTE-600.webp 600w',
      filename: 'pyV29OSBTE-600.webp',
      outputPath: '/home/magnus/cloud/src/sites/magnus.anderssen.ch/dist/longs/critical-resources/pyV29OSBTE-600.webp'
    }
  ]
}
*/
		const { height, width, url } = metadata.webp[0];
		token.attrSet('src', url);
		token.attrSet('height', height);
		token.attrSet('width', width);
		token.attrSet('loading', 'lazy');
		if (metadata.webp[1] && metadata.webp[1].width > width) {
			token.attrSet('class', 'click-to-see');
			token.attrSet('data-full-size-url', metadata.webp[1].url);
			token.attrSet('data-full-size-width', metadata.webp[1].width);
		}
	} catch (e) {
		console.log(`💣 Failed to generate ${path.basename(imgRelativePath)}`);
		console.error(e);
	}
	/*
We want this:
<picture>
  <source type="image/webp" srcset="flower.webp">
  <source type="image/jpeg" srcset="flower.jpg">
  <img src="flower.jpg" alt="">
</picture>
*/

	return slf.renderToken(tokens, idx, options);
};

module.exports = md;
