module.exports = function (url) {
  const base = require("../../content/data/metadata").url;

  return `${base}${url}`.replace(/\/\//g, "/");
};
