module.exports = function (str) {
  if (str === "") {
    return str;
  }

  const npm = /npmjs.com\/package\/(.*)$/.exec(str);

  if (npm) {
    return `npm/${npm[0]}`;
  }

  const split = str.split("/");

  const sitename = split.slice(2, 3);

  if (split.length <= 4) {
    // domain
    return `https://${sitename}`;
  }

  if (str.length < 40 && split.length <= 5) {
    // domain + simple apth
    return str;
  }

  let last =
    split[split.length - 1] !== ""
      ? split[split.length - 1]
      : split[split.length - 2];

  if (last.length > 20) {
    const s = last.split("");
    last = `${s.slice(0, 20).join("")} ⋯`;
  }

  return `https://${sitename} ⋯ ${last}`;
};
