module.exports = function (str) {
  try {
    const date = new Date(str);
    return date.toISOString().split("T")[0];
  } catch (e) {
    console.log(str, e);
    return "";
  }
};
