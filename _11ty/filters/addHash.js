const hasha = require('hasha');
const {readFile} = require('fs/promises')

const addHashCache = {};

module.exports = async function (absolutePath, callback) {
  if (process.env.NODE_ENV !== "production") {
    callback(null, absolutePath);
  } else if (addHashCache[absolutePath]) {
    callback(null, addHashCache[absolutePath]);
  } else {
    try {
      const content = await readFile(`dist${absolutePath}`, {
        encoding: "utf-8",
      });
      const hash = await hasha.async(content);

      addHashCache[absolutePath] = `${absolutePath}?hash=${hash.substr(0, 10)}`;

      callback(null, addHashCache[absolutePath]);
    } catch (e) {
      callback(e);
    }
  }
};
