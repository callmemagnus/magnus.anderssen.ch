module.exports = {
	content: ['./templates/*.njk', './templates/**/*.njk', './content/*.njk', './content/*.html', './src/js/**/*.svelte'],
	// darkMode: 'class'false, // or 'media' or 'class',
	theme: {
		fontFamily: {
			sans: 'Fira',
			serif: 'BreeSerif',
			mono: 'FiraMono'
		},
		container: {
			center: true
		},
		extend: {}
	},
	variants: {},
	plugins: []
};
