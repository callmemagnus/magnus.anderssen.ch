// example.spec.ts
import { test, expect } from '@playwright/test';

// test('example test', async ({ page }) => {
//   await page.goto('https://playwright.dev');
//   expect(await page.screenshot()).toMatchSnapshot('landing.png');
// });

test('Home page', async ({ page }) => {
	await page.goto('http://localhost:8080/index.html');
	await expect(page).toHaveScreenshot({ fullPage: true, mask: [page.locator('time')] });
});

test('404', async ({ page }) => {
	await page.goto('http://localhost:8080/404.html');
	await expect(page).toHaveScreenshot({ fullPage: true, mask: [page.locator('time')] });
});

// test('notes sample', async ({ page }) => {
// 	await page.goto('http://localhost:8080/notes/angular/index.html');
// 	await expect(page).toHaveScreenshot({ fullPage: true, mask: [page.locator('time')] });
// });

test('youtube', async ({ page }) => {
	await page.goto('http://localhost:8080/minis/formik_for_react_forms/');
	await page.waitForTimeout(5000);
	await expect(page).toHaveScreenshot({ fullPage: true, mask: [page.locator('time'), page.locator('.youtube')] });
});
