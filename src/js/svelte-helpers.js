export function mountReplace(Component, options) {
	const frag = document.createDocumentFragment();
	const component = new Component({ ...options, target: frag });

	options.target.parentNode.replaceChild(frag, options.target);

	return component;
}

export function emptyAndMount(Component, options) {
	options.target.innerText = '';

	return new Component(options);
}
