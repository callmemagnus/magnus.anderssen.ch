const times = document.getElementsByTagName('time');
const now = Date.now();

const milliSecondsInAnHour = 1000 * 60 * 60;
const milliSecondsInADay = milliSecondsInAnHour * 24;
const milliSecondsInAMonth = milliSecondsInADay * 30;
const milliSecondsInAYear = milliSecondsInADay * 365;

Array.from(times).forEach(time => {
	if (!time.dateTime || !/\d{4}-\d{2}-\d{2}/.test(time.dateTime)) {
		return;
	}

	const delta = now - new Date(time.dateTime).getTime();
	const yearsAgo = Math.floor(delta / milliSecondsInAYear);
	const monthsAgo = Math.floor(delta / milliSecondsInAMonth);
	const daysAgo = Math.floor(delta / milliSecondsInADay);

	let humanText = `${daysAgo} days ago`;

	if (daysAgo === 0) {
		humanText = 'today';
	} else if (daysAgo === 1) {
		humanText = 'yesterday';
	} else if (daysAgo < 8) {
		humanText = 'last week';
	} else if (daysAgo < 30) {
		humanText = 'last month';
	} else if (daysAgo < 90) {
		humanText = `${monthsAgo} month${monthsAgo > 1 ? 's' : ''} ago`;
	} else if (daysAgo > 365) {
		humanText = `${yearsAgo} year${yearsAgo > 1 ? 's' : ''} ago`;
	}

	time.innerText = `${time.innerText} (${humanText})`;
});
