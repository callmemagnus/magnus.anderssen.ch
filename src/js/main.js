import Timings from '@magnus/send-timings';

import './ago';
import Offline from './offline.svelte';

import '../styles/index.css';

import { emptyAndMount } from './svelte-helpers';
console.log("It's official, you are a real hacker");

if (process.env.NODE_ENV === 'production') {
	if ('serviceWorker' in navigator) {
		window.addEventListener('load', () => {
			navigator.serviceWorker
				.register('/service-worker.js')
				// .then((registration) => {
				//   console.log("SW registered: ", registration);
				// })
				.catch(registrationError => {
					console.log('SW registration failed: ', registrationError);
				});
		});
	}
}

const now = new Date();

new Offline({ target: document.body, props: { label: 'Internet is off :-)' } });

const isXmas = date => date.getMonth() === 11 && date.getDate() >= 15 && date.getDate() < 26;

if (isXmas(now)) {
	import('./xmas.svelte').then(Xmas => {
		new Xmas.default({ target: document.body });
	});
}

const contact = document.getElementsByClassName('contact-popin');
if (contact.length) {
	import('./contact.svelte').then(Contact => {
		for (let i = 0; i < contact.length; i++) {
			const element = contact.item(i);
			emptyAndMount(Contact.default, {
				target: element,
				props: {
					content: element.textContent,
					email: process.env.email,
					phone: process.env.phone,
					rr: process.env.rr
				}
			});
		}
	});
}

const clickToSees = document.getElementsByClassName('click-to-see');
if (clickToSees.length) {
	import('./click-to-see.svelte').then(ClickToSee => {
		for (let i = 0; i < clickToSees.length; i++) {
			const element = clickToSees.item(i);
			const { width, height, alt, src } = /** @type HTMLImageElement */ element;
			const { fullSizeUrl, fullSizeWidth } = /** @type HTMLImageElement */ element.dataset;
			emptyAndMount(ClickToSee.default, {
				target: element.parentNode,
				props: {
					width,
					height,
					fullSizeUrl,
					alt,
					fullSizeWidth,
					src
				}
			});
		}
	});
}

const back = document.getElementsByClassName('back-button');
if (back.length) {
	import('./back-button.svelte').then(BackButton => {
		for (let i = 0; i < back.length; i++) {
			const element = back.item(i);
			emptyAndMount(BackButton.default, {
				target: element,
				props: {
					content: element.textContent
				}
			});
		}
	});
}

const shares = document.getElementsByClassName('share');
if (shares.length) {
	import('./share.svelte').then(Share => {
		for (let i = 0; i < shares.length; i++) {
			const element = shares.item(i);
			const { title, url } = /** @type HTMLSpanElement */ element.dataset;
			emptyAndMount(Share.default, {
				target: element,
				props: {
					title,
					url
				}
			});
		}
	});
}

const code = document.querySelectorAll('code');
if (code.length) {
	import(/* webpackChunkName: "chunk-prism" */ '../styles/prism.css');
}

const timingSender = Timings(process.env.timingsUrl);

document.addEventListener('DOMContentLoaded', () => {
	performance.mark('DOMContentLoaded');
});

document.body.addEventListener('keypress', ({ key }) => {
	if (key === 'Tab') {
		document.body.classList.add('outline');
	}
});

const terminationEvent = 'onpagehide' in self ? 'pagehide' : 'unload';
window.addEventListener(terminationEvent, () => {
	timingSender.sendMeasures();
});

performance.mark('end-of-code');
