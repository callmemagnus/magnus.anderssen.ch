/**
 *
 * @param {number} precision
 */
export default function isMobile(precision = 3) {
	let counter = 0;

	// User agent
	if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
		counter += 1;
	}
	// screen size
	if (window.matchMedia('only screen and (max-width: 760px)').matches) {
		counter += 1;
	}
	// touch
	if ('ontouchstart' in document.documentElement && navigator.userAgent.match(/Mobi/)) {
		counter += 1;
	}
	return precision <= counter;
}
