require('dotenv').config();

const path = require('path');
const webpack = require('webpack');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const TerserPlugin = require('terser-webpack-plugin');
const { WebpackManifestPlugin } = require('webpack-manifest-plugin');
const WorkboxPlugin = require('workbox-webpack-plugin');

const OUT = path.resolve(__dirname, process.env.WEBPACK_OUT);

const email = process.env.email || 'example@example.com';
const phone = process.env.phone || '098 765 43 21';
const timingsUrl = process.env.timingsUrl || '/no-cache/';

const rr = Math.floor(1 + Math.random() * 5);

const obfuscate = str =>
	str
		.split('')
		.map(x => String.fromCharCode(x.charCodeAt(0) - rr))
		.join('');

const defineSafe = JSON.stringify;

const obfuscatedEmail = obfuscate(email);
const obfuscatedPhone = obfuscate(phone);

module.exports = function (_, options) {
	const isDev = options.mode !== 'production';

	return {
		resolve: {
			// see below for an explanation
			alias: {
				svelte: path.resolve('node_modules', 'svelte')
			},
			extensions: ['.mjs', '.js', '.svelte'],
			mainFields: ['svelte', 'browser', 'module', 'main'],
			conditionNames: ['svelte']
		},
		entry: ['./src/js/main.js'],
		output: {
			filename: '[name].js',
			chunkFilename: '[contenthash].js',
			publicPath: '/',
			path: OUT
		},
		devtool: 'source-map',
		plugins: [
			new WebpackManifestPlugin(),
			new MiniCssExtractPlugin({
				filename: '[name].css'
			}),
			new CopyWebpackPlugin({
				patterns: [
					{
						from: 'src/images/icons',
						to: 'icons'
					}
				]
			}),
			new webpack.DefinePlugin({
				'process.env.email': defineSafe(obfuscatedEmail),
				'process.env.phone': defineSafe(obfuscatedPhone),
				'process.env.timingsUrl': defineSafe(timingsUrl),
				'process.env.rr': defineSafe(rr)
			}),
			isDev && new webpack.HotModuleReplacementPlugin(),
			!isDev &&
				new WorkboxPlugin.GenerateSW({
					// these options encourage the ServiceWorkers to get in there fast
					// and not allow any straggling "old" SWs to hang around
					clientsClaim: true,
					skipWaiting: true,
					mode: options.mode
				})
		].filter(x => x),
		optimization: {
			minimize: true,
			minimizer: [
				new TerserPlugin({
					extractComments: true
				})
			]
		},
		module: {
			rules: [
				{
					test: /\.css$/,
					use: [MiniCssExtractPlugin.loader, 'css-loader', 'postcss-loader']
				},
				{
					test: /\.js$/,
					// exclude: /node_modules/,
					use: ['babel-loader']
				},
				{
					test: /\.svelte$/,
					use: {
						loader: 'svelte-loader',
						options: {
							preprocess: {
								// style: sass({
								//   includePaths: ["src"],
								// }),
							}
						}
					}
				},
				{
					// required to prevent errors from Svelte on Webpack 5+, omit on Webpack 4
					test: /node_modules\/svelte\/.*\.mjs$/,
					resolve: {
						fullySpecified: false
					}
				},
				{
					test: /\.woff2?$/,
					// use: ["file-loader"],
					type: 'asset/resource'
				}
			]
		}
	};
};
