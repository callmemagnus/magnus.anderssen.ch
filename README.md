# my personal website

Notes for future me.

## Configuration

With environment variables you can configure:

- **email** -- the email used in the "email me" link
- **phone** -- the phone used in the "call me" link
- **timingsUrl** -- url on which to send performance API metrics

## Publication rules

Because I'm short in memory slots available.

All pages can be:

- draft -- this is a killer switch if true, the page will not be listed or notinmix but it will be published with an ugly banner (defaults to false)
- listed -- whether this page should appear in the list of it's kind (defaults to true)
- notinmix -- whether it should **not** be in the /mix page which aggregates (defaults to true)

## dev

```
npm start
```

## Build

```
npm run build
```

## Deployment

The site is deployed with Jenkins periodically.

## Notes

There is a simple à-la ROT13 encryption mechanism for phone and email.

- Encryption happens in the webpack build.
- Decryption happens on click on the UI elements.

Uses

- webpack to create css, js (vanilla and svelte)
- [eleventy](https://www.11ty.io/) to build the static site based on js templates

To embed twitter content, it is old school:

- use https://publish.twitter.com/

To embed youtube content, add this on its own line:

```
::: youtube oiNtnehlaTo :::
```
