# Todos

## Reduce complexity

Only `webpack` and `11ty` are needed.

Have webpack build js css in `dist` directly.
Have eleventy use webpack manifest to obtain the webpack resources.

See https://github.com/clenemt/eleventy-webpack

## Upgrades

- remove sass
