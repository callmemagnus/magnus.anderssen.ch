#!/usr/bin/env node

const fs = require("fs");
const path = require("path");

function usageAndExit(msg) {
  msg
    ? console.error(msg)
    : console.error("Usage: ./bin/new.js <mini|long|note> <title>");
  process.exit(1);
}

function buildISODate() {
  const now = new Date();
  const year = now.getFullYear();
  const m = now.getMonth();
  const d = now.getDate();
  const month = m < 10 ? `0${m + 1}` : m + 1;
  const day = d < 10 ? `0${d}` : d;
  return [year, month, day].join("-");
}

function buildContent(type, date, title) {
  const content = ["---"];

  content.push(`title: ${title}`);
  content.push(`date: ${date}`);
  type === "mini" && content.push("url: http://...");
  content.push('summary: "Write some summary..."');
  content.push("listed: false");
  content.push("draft: true");
  content.push("notinmix: true");
  content.push("---");
  content.push(`
write something...
  `);

  return content.join("\n");
}

if (process.argv.length < 4) {
  usageAndExit();
}

const [node, me, type, ...titleParts] = Array.from(process.argv);
const filename = titleParts.join("_").toLowerCase();

if (["long", "mini", "note"].indexOf(type) < 0) {
  usageAndExit();
}

console.log(`Creating a new ${type} named ${filename}.md`);

const filePath = path.resolve(`./content/${type}s/${filename}.md`);
const content = buildContent(type, buildISODate(), titleParts.join(" "));

if (fs.existsSync(filePath)) {
  usageAndExit("File already exists.");
}

// console.log(content);
fs.writeFileSync(filePath, content);
