#!/bin/bash

set +x
SITE=magnus.anderssen.ch

if [[ "$email" = "" ]];
then
	echo "Missing email as environment variable."
	echo "Use the following syntax:"
	echo "  export email=XXX"
	exit
fi
if [[ "$phone" = "" ]];
then
	echo "Missing phone as environment variable."
	echo "Use the following syntax:"
	echo "  export phone=XXX"
	exit
fi

docker build --build-arg email --build-arg phone -t ${SITE} . && \
docker image tag ${SITE} registry.magooweb.com/${SITE} && \
docker push registry.magooweb.com/${SITE}

