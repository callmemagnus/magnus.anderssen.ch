


check_for_changes() {
	return $(git status --porcelain | wc -l)
}

check_for_changes

if [[ "$?" != "0" ]];
then
	echo "There are pending changes in the directory, aborting."
	exit 1
fi

echo "No changes detected, running dependency upgrades"
npm run upgradeDeps

check_for_changes

if [[ "$?" != "0" ]];
then
	echo "Creating commit"
	git add package*
	git commit -m "chore: update dependencies"
	echo "you only need to push your changes now"
fi
